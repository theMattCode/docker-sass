[![build status](https://gitlab.com/Betazoid/docker-sass/badges/master/build.svg)](https://gitlab.com/Betazoid/docker-sass/commits/master)

# Usage

The command that is executed by default is ```sass --watch /var/scss/in:/var/scss/out```. So just bind your host folders to be watched to container folders. A container setup may look like:

``` bash
docker run -v /my/app/scss:/var/scss/in -v /my/app/css:/var/scss/out -i -t registry.gitlab.com/betazoid/docker-sass
```

(Unfortunately files cannot be bound as volumes. `watch` will only work watching directories - not files)
