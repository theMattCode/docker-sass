FROM ubuntu:xenial

RUN apt-get update && \
    apt-get install -y ruby && \
    gem install sass -v 3.3.14 && \
    mkdir -p /var/scss/in && \
    mkdir -p /var/scss/out

VOLUME ["/var/scss/in", "/var/scss/out"]

CMD ["sass", "--watch", "/var/scss/in:/var/scss/out"]
